const express = require('express');
const app = express();
const { connectDB } = require('./config/db');

const dotenv = require('dotenv').config();

app.use(express.json());

// routes for pitches
app.use('/api/pitches', require('./routes/pitchRoutes'));

// setup express server
const port = process.env.PORT || 8081;
( async () => {
    await connectDB();
    app.listen(port, () => {
        console.log(`Server listening on ${port}`);
    })
})();